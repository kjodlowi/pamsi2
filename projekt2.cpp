#include <iostream>
#include <string>
#include <fstream>
#include <chrono>

#include "sortowanie.hh"

using namespace std;
using namespace std::chrono;

int read(FILE *plik,element elem[],int count) // funkcja czytania filmu i jego oceny
{
     int counter = 0;
     char c = 'a', c1 = 'a'; // pomocnicze znaki do czytania tytulow
     double rating; // pomocnicza wartosc do zapisywania oceny

     if (!plik)
     {
          cout << "Blad przy otwieraniu pliku!";
          exit(1);
     }

     while (c != '\n')
     {
          c = getc(plik);  // pozbywa sie pierwszej linijki, ktora wyjasnia format pliku csv
     }
     
     for (int   i = 0; i < count; i++)
     {
          fscanf(plik,"%lf", &rating); // usuwa liczbe porzadkowa
          c = getc(plik); // usuwa przecinek po liczbie porzadkowej
          c = getc(plik); // sprawdza pierwszy znak w tytule filmu

          if( c == -1) {return counter;} // jesli koniec pliku zakoncz dzialanie funkcji
          if (c == 34) //sprawdza czy w nazwie wystepuje cudzyslow, nastepnie czyta do momentu wystapienia przecinka po cudzyslowie
          {
               c1 = getc(plik);
               do
               {
                    c = c1;
                    c1 = getc(plik);
                    elem[i].name += c;
               } while ((c != 34) || (c1 != 44));
               elem[i].name.pop_back(); // usuwa z nazwy niepotrzebnie dodany cudzyslow 
          }else // w innym przypadku czyta do nastepnego przecinka
          {
               do
               {
                    elem[i].name += c;
                    c = getc(plik);
               } while (c != 44); // dodaje znaki c do stringa name do momentu napotkania przecinka 
          }

          c = getc(plik);
          ungetc(c,plik);

          if (c != 10 && c != 32) // jesli napotka ocene, zostaje ona dodana do double ocena
          {
               fscanf(plik,"%lf", &rating);
               elem[i].rating = rating;
               counter++;

          }else // jesli nie ma oceny czysci nazwe oraz
                // cofa sie w tablicy o jeden by w nastepnej petli byc w tym samym miejscu
          {
               elem[i].name.clear();
               --i;
          }
     }
     return counter;
} 

bool check(element elem[], int count) //sprawdza czy lista jest dobrze uporzadkowana
{
     bool flag = 0;

     for (int i = 0; i < count-1; i++)
     {
          if (elem[i].rating > elem[++i].rating) // sprawdza czy nastepny element listy jest mniejszy od poprzedniego
          {
               flag = 1; //jesli jest, program o tym informuje
          }
     }
     return flag;
}

int  main()
{
     FILE *plik = fopen("projekt2_dane.csv","r");

     char c = 'a';

     int tab_size = 10000, sort_t = 1, data_d = 0;
     string sort_type = "QuickSort", data_disp = "Nie";

     while (c != 8)
     {
          cout << "Menu" << endl
          << "------------------" << endl
          << "1. Rozmiar tablicy: " << tab_size << endl
          << "2. Rodzaj sortowania: " << sort_type << endl
          << "3. Wyswietlanie posortowanych danych: " << data_disp << endl
          << "4. Posortuj" << endl
          << "5. Zakoncz dzialanie" << endl;

          cin >> c;

          switch (c)
          {
          case '1':
               cout << endl << "Wybierz rozmiar tablicy:" << endl
               << "1. 10 000" << endl
               << "2. 100 000" << endl
               << "3. 500 000" << endl
               << "4. 1 000 000" << endl;
               cin >> c;

               switch (c)
               {
               case '1':
                    tab_size = 10000;
                    break;

               case '2':
                    tab_size = 100000;
                    break;
               
               case '3':
                    tab_size = 500000;
                    break;
               
               case '4':
                    tab_size = 1000000;
                    break;
               
               default:
                    cout << "Wybierz poprawna opcje!" << endl;
                    break;
               }
               break; 
          case '2':
               cout << endl << "Wybierz rodzaj sortowania:" << endl
               << "1. QuickSort" << endl
               << "2. MergeSort" << endl
               << "3. BucketSort" << endl;

               cin >> c;

               switch (c)
               {
               case '1':
                    sort_type = "QuickSort";
                    sort_t = 1;
                    break;

               case '2':
                    sort_type = "MergeSort";
                    sort_t = 2;
                    break;
               
               case '3':
                    sort_type = "BucketSort";
                    sort_t = 3;
                    break;

               default:
                    cout << "Wybierz poprawna opcje!" << endl;
                    break;
               }
               break;
          case '3':
               cout << endl << "Czy wyswietlic posortowana tablice?" << endl
               << "1. Tak" << endl
               << "2. Nie" << endl;
               cin >> c;

               switch (c)
               {
               case '1':
                    data_disp = "Tak";
                    data_d = 1;
                    break;

               case '2':
                    data_disp = "Nie";
                    data_d = 0;
                    break;
               
               default:
                    cout << "Wybierz poprawna opcje!" << endl;
                    break;
               }
               break;
          case '4':
               c = 8;
               break;
          
          case '5':
               return 0;
               break;
          
          default:
               cout << "Wybierz poprawna opcje!" << endl;
               break;
          }    
     }
     
     int count = tab_size;

     element * elem = new element[tab_size];

     auto start1 = high_resolution_clock::now();
     count = read(plik,elem,count);
     auto stop1 = high_resolution_clock::now();


     auto start = high_resolution_clock::now();
     switch (sort_t)
     {
     case 1:
          QuickSort(elem,0,count-1);
          break;
     
     case 2:
          MergeSort(elem,0,count-1);
          break;
     
     case 3:
          BucketSort(elem,10,count);
          break;
     }
     auto stop = high_resolution_clock::now();
     
     //HeapSort(elem,count);

     auto duration = duration_cast<microseconds>(stop-start);
     auto duration1 = duration_cast<microseconds>(stop1-start1);
     
     if (data_d)
     {
          for (int   i = 0; i < count; i++)
          {
               cout << "Lp: "<< i+1 << "| Ocena: " << elem[i].rating << "| Nazwa: " << elem[i].name << endl;
          } 
     }

     cout << endl << "Czytanie trwalo: " << duration1.count() << " mikrosekund" << endl;
     cout << "Sortowanie trwalo: " << duration.count() << " mikrosekund" << endl;
     cout << "Ilosc elementow: " << count << endl;
     cout << "Mediana: " << median(elem,count) << endl;
     cout << "Srenia: " << avg(elem,count) << endl;
     cout << "Sprawdzam poprawnosc sortowania: " << check(elem,count) << " (1 - wykryto blad, 0 - brak bledow) " << endl;

     delete[] elem;

     system("pause");
     return 0;
}