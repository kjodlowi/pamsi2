#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

/**
 * * Pojedynczy element, sklada sie z stringa z nazwa filmu oraz inta z ocena filmu
 */
struct element
{
    std::string name; // nazwa filmu
    int rating; // ocena filmu
};

/**
 *  * Mediana, podajemy tablice elementow oraz liczbe elementow w tablicy
 */
double median(element array[], int count)
{
    double tmp = count%2; // sprawdza czy liczba elementow jest parzysta
    if (tmp < 1)
    {
        tmp = (array[count/2].rating + array[(count/2)+1].rating)/2;  //jesli tak - liczy srednia z dwoch srodkowych liczb
    }else
    {
        tmp = array[count/2].rating;  // jesli tak - wyswietla srodkowa liczbe
    }
    return tmp;
}

/**
 *  * Srednia, podajemy tablice elementow oraz ich liczbe w tablicy
 */
double avg(element array[], int count)
{
    double avg = 0;
    for (int i = 0; i < count; i++)
    {
        avg += array[i].rating;
    }
    return (avg/count);
}

/**
 *  * Sortowanie szybkie, sortuje w kolejnosci rosnacej, przy wywolaniu podajemy tablice elementow, poczatkowy oraz koncowy indeks tablicy
 *  Zlozonosc czasowa O(nlogn)  dla wiekszosci przypadkow, O(n^2) dla najgorszego przypadku
 */
void QuickSort(element array[], int   left, int   right)
{
    element tmp; // pomocniczy element do zamiany
    int   i = left;
    int   j = right;
    int   pivot = array[(left + right) / 2].rating; //wyznaczanie pivota z srodka tablicy
    while (i < j) // dziala do momentu dojscia do pivota
    {
        while (array[i].rating < pivot) i++; // jesli element i jest po dobrej stronie, przechodzi do nastepnego
        while (array[j].rating > pivot) j--; // jesli element j jest po dobrej stronie, przechodzi do poprzedniego 

        if (i <= j) //jesli sa nieposortowane, porgram zamienia elementy miejscami
        {
            tmp = array[i]; //zamienia miejscami elementy tablicy 
            array[i++] = array[j];
            array[j--] = tmp;
        }
    }
    if (j > left) QuickSort(array, left, j); // rekurencyjne wywolanie funkcji dla lewej strony od pivota
    if (i < right) QuickSort(array, i, right); // rekurencyjne wywolanie funkcji dla prawej strony od pivota
}


/**
 *  * Merge - scalanie, podajemy tablice elementow, poczatkowy indeks, srodek tablicy oraz koncowy indeks
 * rozbija glowna tablice na pomocnicze oraz sortuje je i scala
 */
void merge(element array[], int left, int mid, int right)
{
    int n1 = mid - left + 1, n2 = right - mid; // obliczamy potrzebne wielkosci pomocniczych tablic

    element *L = new element[n1], *R = new element[n2]; // pomocnicze wskazniki na tablice dla elementow z lewej oraz prawej strony

    for (int i = 0; i< n1; i++)
    {
        L[i] = array[left+i]; // wypelniamy obie pomocnicze tablice wartosciami z oryginalnej tablicy
    }
    for (int i = 0; i < n2; i++)
    {
        R[i] = array[mid + 1 + i];
    }
    
    int i = 0, j = 0, k = left; // poczatkowe indeksy tablic pomocniczych oraz scalonej tablicy
    
    while (i < n1 && j < n2) // scalanie pomocniczych tablic
    {
        if (L[i].rating <= R[j].rating)
        {
            array[k] = L[i];
            i++;
        }else
        {
            array[k] = R[j];
            j++;
        }
        k++;
    }

    while(i < n1)  //jesli w pomocniczych tablicach zostaly jakies elementy, dodajemy je do glownej tablicy
    {
        array[k] = L[i];
        i++;
        k++;
    }
    while (j < n2) 
    {
        array[k] = R[j];
        j++;
        k++;
    }
    delete[] R; // zwalnianie zaalokowanego miejsca w pamieci
    delete[] L;
}

/**
 *  * Sortowanie przez scalanie, podajemy tablice elementow, poczatkowy oraz koncowy indeks
 */
void MergeSort(element array[], int left, int right)
{
    if (left >= right) return; // po skonczonym sortowaniu funkcja sie konczy

    int mid = left + (right-left)/2; // wyznaczanie srodka tablicy

    MergeSort(array,left,mid); // rekurencyjne wywolanie funkcji dla dwoch czesci tablicy
    MergeSort(array,mid+1,right);

    merge(array,left,mid,right); // sortowanie oraz scalanie tablicy wzgledem srodka
}

/**
 * * Sortowanie kubelkowe, podajemy liczbe kubelkow oraz wielkosc tablicy
 * Nie posiada funkcji sortowania wektorow, poniewaz w tym zadaniu jest to tylko strata czasu, poniewaz wartosci sa intami
 * Zlozonosc obliczeniowa O(n+N) (n - liczba kubelkow, N - liczba danych w tablicy)
 */
void BucketSort(element array[], int n, int count)
{
    vector<element> *bucket = new vector<element>[n]; // pomocnicza tablica wektorow

    for (int i = 0; i < count; i++)
    {
        bucket[array[i].rating-1].push_back(array[i]); //dodawanie elementow z wejsciowej tablicy do odpowiedniego pomocniczego wektora
    }

    int index = 0; // pomocnicza wartosc sluzaca do wypelnienia gotowej tablicy

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < bucket[i].size(); j++)
        {
            array[index++] = bucket[i][j]; // zwraca wartosci z kolejnych kubelkow do poczatkowej tablicy
        }
    }
    delete[] bucket; //usuwamy pomocnicza tablice wektorow
}

/*
void heapify(element array[], int n, int i)
{
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < n && array[left].rating > array[largest].rating)
    {
        largest = left;
    }

    if (right < n && array[right].rating > array[largest].rating)
    {
        largest = right;
    }

    if (largest != i)
    {
        swap(array[i].rating,array[largest].rating);
        swap(array[i].name,array[largest].name);
        heapify(array, n, largest);
    } 
}

void HeapSort(element array[], int n)
{
    for (int i = n/2 - 1; i >= 0; i--)
    {
        heapify(array, n, i);
    }
    
    for (int i = n - 1; i > 0; i--)
    {
        swap(array[0].rating,array[i].rating);
        swap(array[0].name,array[i].name);

        heapify(array,i,0);
    }
}
*/